const { skip } = require('graphql-resolvers');
const { ForbiddenError } = require('apollo-server');

const isAuthenticated = (_, args, { req: { userId } }) => 
    userId ? skip : new ForbiddenError('Not Authenticated as a user.');

module.exports = isAuthenticated;