const cors = require('cors');

module.exports = function(app) {
    app.use(
        cors({
            origin: [process.env.FRONTEND_URI],
            credentials: true,
            optionsSuccessStatus: 200,
        })
    );
};