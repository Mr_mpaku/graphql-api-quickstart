const models = require('../models');

module.exports = function(app){
    // This middleware populates the user object on each request
    app.use(async (req, res, next) => {
        // if the user is not logged in, skip this
        if (!req.userId) return next();

        const user = await models.user.findById(req.userId);
        req.user = user;
        next();
    });
};
