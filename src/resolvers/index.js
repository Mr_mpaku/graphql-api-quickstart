const { GraphqlDateTime } = require('graphql-iso-date');

const userResolver = require('./user');

module.exports = [userResolver];