const bcrypt = require('bcryptjs');
const { AuthenticationError, UserInputError } = require('apollo-server');
const { randomBytes } = require('crypto');
const { promisify } = require('util');
const { combineResolvers } = require('graphql-resolvers');
const isAuthenticated = require('../middlewares/authorization');

const setCookie = function(res, token) {
    res.cookie('token', token, {
        // Javascript shouldn't be able to access the cookie
        httpOnly: true,
        // One year cookie
        maxAge: 1000 * 60 * 24 * 7,
    });
};

module.exports = {
    Query: {
        me: combineResolvers(isAuthenticated, async (_, args, { req, models }) =>
            models.User.findById(req.userId)
        ),
        user: async (_, { id }, { models: { User } }) => User.findById(id),
        users: async (_, args, { models: { User } }) => User.find(), 
    },

    Mutation: {
        signup: async (_, { firstName, lastName, email, password }, { models, res }) => {
            // lowercase the email
            email = email.toLowerCase();
            // create the user in the database
            const user = new models.User({
                firstName,
                lastName,
                password,
                email,
            });

            try {
                await user.save();
            } catch (error) {
                throw new Error(error.message);
            }

            // create the jwt
            try {
                const token = await user.generateToken();
                // set the jwt as a cookie on the response
                setCookie(res, token);
            } catch (error) {
                throw new Error(error.message);
            }

            // return the user
            return user;
        },
        signin: async (_, { email, password }, { models, res }) => {
            try {
                // check if there is a user with that email
                const user = await models.User.findOne({ email });

                if (!user) {
                    throw new UserInputError('No user found with these login credentials.');
                }

                // check if the user's password is correct
                const isValid = await user.validatePassword(password);
                
                if (!isValid) {
                    throw new AuthenticationError('Your login details are incorrect.')
                }

                // create the jwt 
                const token = await user.generateToken();
                // set the jwt as a cookie on the response
                setCookie(res, token);
                
                // return the user
                return user;
            } catch (error) {
                throw new Error(error.message);
            }
        },
        signout: async (_, args, { res }) => {
            res.clearCookie('token');
            return { message: 'User logged out successfuly!' };
        },
        requestReset: async (_, { email }, { models }) => {
            try {
                // check if there is a user with that email
                const user = await models.User.findOne({ email });
                if (!user) throw new UserInputError(`No user was found for the email ${email}.`);

                // set a reset token and its lifespan on that user
                const randomBytesPromsified = promisify(randomBytes);
                const resetToken = (await randomBytesPromsified(20)).toString('hex');
                const resetTokenExpiry = Date.now() + 3600000; // one hour from now
                await models.User.findOneAndUpdate(
                    { email },
                    {
                        $set: {
                            resetToken,
                            resetTokenExpiry,
                        },
                    },
                    { new: true }
                );
                
                // send email to the given email
                const subject = "Reset Password Request";
                const emailData = {
                    name: user.firstName,
                    title: "Password reset",
                    description: "You have requested a password reset, please follow the link below to reset your password.",
                    link: "NEED ONE", // TODO: put the reset page link here.
                    buttonText: "Follow this link to reset your password.",
                };
                await mail.sendMail(email, subject, emailData);
            } catch (error) {
                throw new Error(error.message);
            }
            return { message: 'Reset Request Sent Successfully!' };
        },
        resetPassword: async (_, { password, confirmPassword, resetToken }, { models, res }) => {
            // check if the passwords match
            if (password !== confirmPassword) throw new Error('Passwords Do Not Match');

            try {
                // check if the token is legit
                const user = await models.User.findOne({ resetToken });
            } catch (error) {
                throw new Error(error.message);
            }

            if (!user) throw new Error('This token is either invalid.');
            
            // and check if the token has expired
            if (!(user.resetTokenExpiry > Date.now() - 3600000)) throw new Error('This token has expired.');
            
            try {
                // hash the new password 
                const newPassword = await bcrypt.hash(password, 10);
                // save the new password to the user and remove the old reset token fields
                const updatedUser = await models.User.findOneAndUpdate(
                    {
                        email: user.email,
                    },{
                        $set: {
                            password: newPassword,
                            resetToken: null,
                            resetTokenExpiry: null,
                        },
                    },
                    { new : true }
                );

                // create the jwt
                const token = await updatedUser.generateToken(); // TODO: Check this - causing an error
                // set the jwt as a cookie on in the response
                setCookie(res, token);
            } catch (error) {
                throw new Error(error.message);
            }
            
            // return the new user
            return updatedUser;
        },
        updateProfile: combineResolvers(isAuthenticated, async (_, args, { req, models }) => {
            // isAuthnticated is middleware that checks if the user accessing this resolver is authenticated.
            const { user } = req;
            // update the user and return the updated user
            return models.User.findOneAndUpdate(
                { 
                    email: user.email 
                },
                {
                    $set: {
                        ...args,
                    },
                },
                { new: true }
            );
        }),
    },
};
