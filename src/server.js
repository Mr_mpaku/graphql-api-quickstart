require('dotenv').config();
const { ApolloServer } = require('apollo-server-express');
const express = require('express');
const models = require('./models');
const schema = require('./schemas');
const resolvers = require('./resolvers');

// Express App
const app = express();

// Middlewares
require('./config/db')();
require('./middlewares/cookieParser')(app);
require('./middlewares/auth')(app);
require('./middlewares/cors')(app);
require('./middlewares/loggedIn')(app);
require('./middlewares/morgan')(app);
require('./middlewares/error')(app);


// Creating the apollo-server
const server = new ApolloServer({
    introspection: true,
    playground: true,
    typeDefs: schema,
    resolvers,
    context: async ({req, res}) => ({
        req,
        res,
        models,
    }),
    resolverValidateOptions: {
        requireResolveForResolveType: false,
    },
});

// This is a middleware function that gets executed in between requests and responses
server.applyMiddleware({
    app,
    cors: {
        origin: [process.env.FRONTEND_URL],
        credentials: true,
        optionsSuccessStatus: 200,
    },
    path: '/',
});

module.exports = app;