const mongoose = require('mongoose');
const { gql } = require('apollo-server-express');

module.exports = gql`
    extend type Query {
        me: User
        users: [User]!
        user(id: ID!): User
    }

    extend type Mutation {
        signup(
            firstName: String!
            lastName: String!
            email: String!
            password: String!
        ): User!
        signin(email: String!, password: String!): User!
        signout: SuccessMessage
        requestReset(email: String!): SuccessMessage
        resetPassword(
            resetToken: String!
            password: String!
            confirmPassword: String!
        ): User!
        updateProfile(
            firstName: String
            lastName: String
        ): User!
    }

    type User {
        id: ID!
        firstName: String!
        lastName: String!
        email: String!
        resetToken: String
        resetTokenExpiry: Float
    }

    type SuccessMessage {
        message: String
    }
`;
